<?php

function simple_crm_conversations_views_data()
{
	$data = array();

	$data['simple_crm_emails']['table']['group'] = t('Email Messages');

	// Advertise this table as a possible base table
	$data['simple_crm_emails']['table']['base'] = array(
		'field' => 'mid',
		'title' => t('Email Messages'),
		'weight' => 0,
		'defaults' => array(
			'field' => 'subject',
		),
	);

	$data['simple_crm_emails']['contact_nid'] = array(
		'title' => t('Contact Nid'),
		'help' => t('The node ID of the contact associated with this message.'),
		'field' => array(
			'handler' => 'views_handler_field_node',
			'click sortable' => TRUE,
			),
		'argument' => array(
			'handler' => 'views_handler_argument_node_nid',
			'name field' => 'title', // the field to display in the summary.
			'numeric' => TRUE,
			'validate type' => 'nid',
			),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
	);

	$data['simple_crm_emails']['from'] = array(
		'title' => 'From Address',
		'help' => t('The sender\'s email address.'),

		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'filter' => array(
			'handler' => 'views_handler_filter_string',
		),
		'argument' => array(
			'handler' => 'views_handler_argument_string',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
	);

	$data['simple_crm_emails']['to'] = array(
		'title' => 'To Address',
		'help' => t('The recepient\'s email address.'),

		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'filter' => array(
			'handler' => 'views_handler_filter_string',
		),
		'argument' => array(
			'handler' => 'views_handler_argument_string',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
	);

	$data['simple_crm_emails']['subject'] = array(
		'title' => 'Subject',
		'help' => t('The subject line.'),

		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'filter' => array(
			'handler' => 'views_handler_filter_string',
		),
		'argument' => array(
			'handler' => 'views_handler_argument_string',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
	);

	$data['simple_crm_emails']['body'] = array(
		'title' => 'Body',
		'help' => t('The body of the email.'),

		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'filter' => array(
			'handler' => 'views_handler_filter_string',
		),
		'argument' => array(
			'handler' => 'views_handler_argument_string',
		),
	);

	$data['simple_crm_emails']['date'] = array(
		'title' => 'Date',
		'help' => t('Date the message was sent.'),

		'field' => array(
			'handler' => 'views_handler_field_date',
			'click sortable' => TRUE,
		),
		'filter' => array(
			'handler' => 'views_handler_filter_date',
		),
		'argument' => array(
			'handler' => 'views_handler_argument_date',
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
	);

	return $data;
}
