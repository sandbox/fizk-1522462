<?php

/**
 * Implementation of hook_views_default_views()
 */
function simple_crm_conversations_views_default_views() {
  $views = array();
  $view = new view;

  $view->name = 'email_messages';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'simple_crm_emails';
  $view->human_name = 'Email Messages';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = TRUE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'views_accordion';
  $handler->display->display_options['style_options']['grouping'] = array(
		  0 => array(
			  'field' => 'subject',
			  'rendered' => 1,
			  'rendered_strip' => 0,
			  ),
		  );
  $handler->display->display_options['style_options']['use-grouping-header'] = 1;
  $handler->display->display_options['style_options']['row-start-open'] = '0';
  $handler->display->display_options['style_options']['autoheight'] = 0;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no messages.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Field: Email Messages: Subject */
  $handler->display->display_options['fields']['subject']['id'] = 'subject';
  $handler->display->display_options['fields']['subject']['table'] = 'simple_crm_emails';
  $handler->display->display_options['fields']['subject']['field'] = 'subject';
  $handler->display->display_options['fields']['subject']['label'] = '';
  $handler->display->display_options['fields']['subject']['exclude'] = TRUE;
  $handler->display->display_options['fields']['subject']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['external'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['subject']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['subject']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['subject']['alter']['html'] = 0;
  $handler->display->display_options['fields']['subject']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['subject']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['subject']['hide_empty'] = 0;
  $handler->display->display_options['fields']['subject']['empty_zero'] = 0;
  $handler->display->display_options['fields']['subject']['hide_alter_empty'] = 1;
  /* Field: Email Messages: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'simple_crm_emails';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['body']['alter']['text'] = '<br />[body]';
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 1;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  /* Field: Email Messages: Date */
  $handler->display->display_options['fields']['date']['id'] = 'date';
  $handler->display->display_options['fields']['date']['table'] = 'simple_crm_emails';
  $handler->display->display_options['fields']['date']['field'] = 'date';
  $handler->display->display_options['fields']['date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['date']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['date']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['date']['date_format'] = 'long';
  /* Sort criterion: Email Messages: Date */
  $handler->display->display_options['sorts']['date']['id'] = 'date';
  $handler->display->display_options['sorts']['date']['table'] = 'simple_crm_emails';
  $handler->display->display_options['sorts']['date']['field'] = 'date';
  $handler->display->display_options['sorts']['date']['order'] = 'DESC';
  /* Contextual filter: Email Messages: From Address */
  $handler->display->display_options['arguments']['from']['id'] = 'from';
  $handler->display->display_options['arguments']['from']['table'] = 'simple_crm_emails';
  $handler->display->display_options['arguments']['from']['field'] = 'from';
  $handler->display->display_options['arguments']['from']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['from']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['from']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['from']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['from']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['from']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['from']['glossary'] = 0;
  $handler->display->display_options['arguments']['from']['limit'] = '0';
  $handler->display->display_options['arguments']['from']['transform_dash'] = 0;
  $handler->display->display_options['arguments']['from']['break_phrase'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'email-messages';

  $views[$view->name] = $view;
  return $views;
}
